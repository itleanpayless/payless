package com.itlean.payless.model;

import java.util.List;

public class Authorization {

	public class TaxesDescription{
	    int iva;
	    int tax;
	}

	public class Product{
	    String retailId;
	    double quantity;
	    double value;
	    int units;
	    double discount;
	}

	public class Root{
	    double totalValue;
	    double totalTaxes;
	    double retailStoreId;
	    TaxesDescription taxesDescription;
	    double totalDiscount;
	    List<Product> products;
	}
}
