package com.itlean.payless.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.*;

@Entity
@Table
@Data
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Builder
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String ean;
	private int quantity;
	private String retailId;
	private Double value;
	private Double units;
	private Double fullValue;
	private Double discountedValue;
	private int measure;
	private Boolean substitutes;
	private Boolean onCatalogo;
	private Boolean fulfillmentRate;

	
}
