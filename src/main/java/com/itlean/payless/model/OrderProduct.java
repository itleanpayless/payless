package com.itlean.payless.model;

import java.util.List;

public class OrderProduct {

	
	public OrderProduct()
	{
		
	}
	public class Order{
	    String orderId;
	    String createAt;
	}	
	public class Payment{
	    int value;
	}

	public class Root{
	    Order order;
	    Client client;
	    List<Product> products;
	    Payment payment;
	}

}
