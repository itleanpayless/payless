package com.itlean.payless.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itlean.payless.model.Product;
import com.itlean.payless.repositories.ProductRepository;

@Service
public class ProductService implements ProductServiceImpl {

	@Autowired
	private ProductRepository productRepository;
	
	public ProductService(ProductRepository productRepository) {
		super();
		this.productRepository = productRepository;
	}

	@Override
	public List<Product> findAll() {
		List<Product> products = (List)this.productRepository.findAll();
		return products;
	}

	@Override
	public Product saveProduct(Product product) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteProduct(Product product) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Product findById(Integer id) {
		Optional<Product> product = this.productRepository.findById(id);
		return product.get();
	}

	@Override
	public List<Product> findAllByName(String ean) {
		List<Product> products = this.productRepository.findByName(ean);
		return products;
	}

}
