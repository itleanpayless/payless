package com.itlean.payless.services;

import java.util.List;

import com.itlean.payless.model.Product;

public interface ProductServiceImpl {
	
	public List<Product> findAll();
	public Product saveProduct(Product product);
	public void deleteProduct(Product product);
	public Product findById(Integer id);
	public List<Product> findAllByName(String ean); 

}
