package com.itlean.payless.controllers;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.itlean.payless.model.Product;
import com.itlean.payless.services.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	public ProductController(ProductService productService) {
		super();
		this.productService = productService;
	}

	@GetMapping
	public ResponseEntity<?> getAll() {
		List<Product> products = this.productService.findAll();
		return !products.isEmpty() ? ResponseEntity.ok(products) : ResponseEntity.noContent().build();
	}
	
	@PostMapping
	public ResponseEntity<?> newProduct(@RequestBody Product model, HttpServletResponse response) {
		Product productSave = this.productService.saveProduct(model);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(productSave.getId()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		return ResponseEntity.created(uri).body(productSave);
	}

	@GetMapping("/{ean}")
	public ResponseEntity<?> getAll(@PathVariable String ean) {
		List<Product> products = this.productService.findAllByName(ean);
		return !products.isEmpty() ? ResponseEntity.ok(products) : ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable int id) {
		Product product = this.productService.findById(id);
		this.productService.deleteProduct(product);
	}
}
