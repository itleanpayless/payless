package com.itlean.payless.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;

@RestController
@RequestMapping("/api")
@Api(value = "Pagina Home")
public class HomeController {

	@ApiOperation(value = "Este é o controler da home")
	@GetMapping(path = "/home")
	public String home() {
		return "Home";
	}
}
