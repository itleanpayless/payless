package com.itlean.payless.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.itlean.payless.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {   
	
	@Query("SELECT p FROM Product p WHERE p.ean = ?1")
	public List<Product> findByName(String ean);
	
}
